import React from "react";
import { Link } from "react-router-dom";
import { ASSETS_ENUM } from "../../utils";

function NotFound() {
  return (
    <div className="w-full mt-36 flex flex-col items-center content-center">
      <h1> 🔪 Lộn chỗ rồi anh 2 à ! </h1>
      <Link to="/home">👉 Click vô để quay lại ở đây nè !</Link>
      <img
        src={ASSETS_ENUM.NOT_FOUND_IMG}
        alt="Girl in a jacket"
        width="500"
        height="600"
      />
    </div>
  );
}

export default NotFound;
