import HomePage from "./Home/Home.page";
import NotFound from "./NotFound/NotFoundPage";

const routes = [
  {
    path: "*",
    exact: true,
    public: true,
    component: <NotFound />,
    routes: [],
  },
  {
    path: "/",
    exact: true,
    public: true,
    component: <HomePage />,
    routes: [],
  },
  {
    path: "/home",
    exact: true,
    public: true,
    component: <HomePage />,
    routes: [],
  },
];
export default routes;
