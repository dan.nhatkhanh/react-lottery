import {
  PresentationChartLineIcon,
  Square3Stack3DIcon,
  UserCircleIcon,
} from "@heroicons/react/24/solid";
import { Tabs } from "@material-tailwind/react";
import React from "react";
import { TabsBodyComponent } from "../../components/TabsBody";
import { TabsHeaderComponent } from "../../components/TabsHeader";
import { Dashboard } from "../../components/Dashboard";
import { Profile } from "../../components/Profile";
import { LatestHistory } from "../../components/History";

function HomePage() {
  console.log("homepage ne");

  const data = [
    {
      label: "Dashboard",
      value: "dashboard",
      icon: <Square3Stack3DIcon className="h-5 w-5" />,
      detail: <Dashboard />,
    },
    {
      label: "Profile",
      value: "profile",
      icon: <UserCircleIcon className="h-5 w-5" />,
      detail: <Profile />,
    },
    {
      label: "History",
      value: "history",
      icon: <PresentationChartLineIcon className="h-5 w-5" />,
      detail: <LatestHistory />,
    },
  ];
  return (
    <Tabs value="dashboard" orientation="vertical">
      <TabsHeaderComponent data={data} />
      <TabsBodyComponent data={data} />
    </Tabs>
  );
}

export default HomePage;
