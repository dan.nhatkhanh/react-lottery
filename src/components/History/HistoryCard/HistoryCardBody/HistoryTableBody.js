import { PencilIcon } from "@heroicons/react/24/solid";
import {
  Avatar,
  Chip,
  IconButton,
  Tooltip,
  Typography,
} from "@material-tailwind/react";
import React from "react";
import { TABLE_ROWS } from "../../HistoryData";

function HistoryTableBody() {
  return (
    <tbody>
      {TABLE_ROWS.map(({ img, name, amount, date, status }, index) => {
        const isLast = index === TABLE_ROWS.length - 1;
        const classes = isLast ? "p-4" : "p-4 border-b border-blue-gray-50";

        return (
          <tr key={name}>
            <td className={classes}>
              <div className="flex items-center gap-3">
                <Avatar
                  src={img}
                  alt={name}
                  size="md"
                  className="border border-blue-gray-50 bg-blue-gray-50/50 object-contain p-1"
                />
                <Typography
                  variant="small"
                  color="blue-gray"
                  className="font-bold"
                >
                  {name}
                </Typography>
              </div>
            </td>
            <td className={classes}>
              <Typography
                variant="small"
                color="blue-gray"
                className="font-normal"
              >
                {amount}
              </Typography>
            </td>
            <td className={classes}>
              <Typography
                variant="small"
                color="blue-gray"
                className="font-normal"
              >
                {date}
              </Typography>
            </td>
            <td className={classes}>
              <div className="w-max">
                <Chip
                  size="sm"
                  variant="ghost"
                  value={status}
                  color={
                    status === "paid"
                      ? "green"
                      : status === "pending"
                      ? "amber"
                      : "red"
                  }
                />
              </div>
            </td>
            <td className={classes}>
              <Tooltip content="Edit User">
                <IconButton variant="text" color="blue-gray">
                  <PencilIcon className="h-4 w-4" />
                </IconButton>
              </Tooltip>
            </td>
          </tr>
        );
      })}
    </tbody>
  );
}

export default HistoryTableBody;
