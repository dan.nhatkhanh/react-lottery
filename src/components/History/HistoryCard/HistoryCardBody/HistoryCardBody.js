import { CardBody } from "@material-tailwind/react";
import React from "react";
import HistoryTableHeader from "./HistoryTableHeader";
import HistoryTableBody from "./HistoryTableBody";

function HistoryCardBody() {
  return (
    <CardBody className="overflow-scroll px-0">
      <table className="w-full min-w-max table-auto text-left">
        <HistoryTableHeader />
        <HistoryTableBody />
      </table>
    </CardBody>
  );
}

export default HistoryCardBody;
