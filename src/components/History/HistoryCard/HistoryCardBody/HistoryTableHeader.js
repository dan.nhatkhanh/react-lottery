import React from "react";
import { TABLE_HEAD } from "../../HistoryData";
import { Typography } from "@material-tailwind/react";

function HistoryTableHeader() {
  return (
    <thead>
      <tr>
        {TABLE_HEAD.map((head) => (
          <th
            key={head}
            className="border-y border-blue-gray-100 bg-blue-gray-50/50 p-4"
          >
            <Typography
              variant="small"
              color="blue-gray"
              className="font-normal leading-none opacity-70"
            >
              {head}
            </Typography>
          </th>
        ))}
      </tr>
    </thead>
  );
}

export default HistoryTableHeader;
