import { CardFooter } from "@material-tailwind/react";
import React, { useState } from "react";
import Pagination from "../../Pagination/Pagination";
import { TABLE_ROWS } from "../HistoryData";

function HistoryCardFooter() {
  const [currentPage, setCurrentPage] = useState(1);
  const paginate = (pageNumber) => {
    setCurrentPage(pageNumber);
  };
  const dataPerPage = 1;
  return (
    <CardFooter className="flex items-center justify-between border-t border-blue-gray-50 p-4">
      <Pagination
        dataPerPage={dataPerPage}
        totalData={TABLE_ROWS?.length}
        paginate={paginate}
        currentPage={currentPage}
      />
    </CardFooter>
  );
}

export default HistoryCardFooter;
