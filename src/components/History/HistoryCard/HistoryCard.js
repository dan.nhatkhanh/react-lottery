import { Card } from "@material-tailwind/react";
import React from "react";
import HistoryCardBody from "./HistoryCardBody/HistoryCardBody";
import HistoryCardFooter from "./HistoryCardFooter";
import HistoryCardHeader from "./HistoryCardHeader";

function HistoryCard() {
  return (
    <Card className="h-full w-full">
      <HistoryCardHeader />
      <HistoryCardBody />
      <HistoryCardFooter />
    </Card>
  );
}

export default HistoryCard;
