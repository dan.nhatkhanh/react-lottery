import React, { useState } from "react";
import HistoryCard from "./HistoryCard/HistoryCard";

function LatestHistory() {
  return (
    <div>
      <div className="mb-2">
        <h1 className="mt-5 text-3xl">History</h1>
        <p>View all of your trades and transactions.</p>
      </div>
      <hr className="mb-4 h-[0.2rem] w-96 rounded border-0 bg-gray-300 md:mb-10" />
      <HistoryCard />
    </div>
  );
}

export default LatestHistory;
