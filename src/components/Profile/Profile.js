import React from "react";

function Profile() {
  return (
    <div>
      <h1 className="mt-5 text-3xl">Profile</h1>
      <p>View your profile information.</p>
    </div>
  );
}

export default Profile;
