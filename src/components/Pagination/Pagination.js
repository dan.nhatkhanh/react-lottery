import getPaginationItems from "../../utils/GetPagination/getPaginationItems";

function Pagination({ dataPerPage, totalData, currentPage, paginate }) {
  const maxPageLimit = Math.ceil(totalData / dataPerPage);
  const pageNumberList = getPaginationItems(currentPage, maxPageLimit, 5);

  return (
    <div className="flex flex-wrap items-center justify-between">
      <h1 className="text-slate-700 text-lg">
        Page{" "}
        <span className="text-xl font-semibold">
          {currentPage}/{maxPageLimit}
        </span>
      </h1>
      <div className="page-item my-1 flex">
        <button
          className="bg-slate-300 text-slate-700 disabled:cursor-not-allowed disabled:bg-white"
          disabled={currentPage < 2 && true}
          onClick={() => {
            paginate(--currentPage);
          }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="h-6 w-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M15.75 19.5L8.25 12l7.5-7.5"
            />
          </svg>
        </button>
        {pageNumberList.map((number) => {
          return !isNaN(number) ? (
            <button
              key={number}
              style={{
                boxShadow:
                  "rgba(50, 50, 93, 0.25) 0px 30px 60px -12px inset, rgba(0, 0, 0, 0.3) 0px 18px 36px -18px inset",
              }}
              onClick={() => paginate(number)}
              className={`mx-2 rounded-full bg-gray-500 px-3 py-1 text-white ${
                currentPage === number && "bg-slate-700"
              }`}
            >
              {!isNaN(number) ? number : "..."}
            </button>
          ) : (
            <p className="text-2xl tracking-widest text-[#40b59e]">...</p>
          );
        })}
        <button
          className="bg-slate-300 text-slate-700 disabled:cursor-not-allowed disabled:bg-white"
          disabled={
            currentPage === pageNumberList[pageNumberList.length - 1] && true
          }
          onClick={() => {
            paginate(++currentPage);
          }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="h-6 w-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M8.25 4.5l7.5 7.5-7.5 7.5"
            />
          </svg>
        </button>
      </div>
    </div>
  );
}

export default Pagination;
