import { TabPanel, TabsBody } from "@material-tailwind/react";
import React from "react";

function TabsBodyComponent({ data }) {
  return (
    <TabsBody className="px-4 py-3">
      {data.map(({ value, detail }) => (
        <TabPanel key={value} value={value} className="py-0">
          {detail}
        </TabPanel>
      ))}
    </TabsBody>
  );
}

export default TabsBodyComponent;
