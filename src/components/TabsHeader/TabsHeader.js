import { ChevronDoubleUpIcon } from "@heroicons/react/24/solid";
import { Tab, TabsHeader } from "@material-tailwind/react";
import React from "react";

function TabsHeaderComponent({ data }) {
  return (
    <TabsHeader className="h-screen w-52 px-4 py-3">
      <div className="mt-5 flex items-center justify-center">
        <ChevronDoubleUpIcon className="mr-2 h-6 w-6 animate-bounce text-gray-800" />
        <h1 className="text-lg font-medium text-gray-700">Wock</h1>
      </div>
      <hr className="mx-auto my-1 h-[0.1rem] w-20 rounded border-0 bg-gray-300 md:my-3" />
      {data.map(({ label, value, icon }) => (
        <Tab key={value} value={value} className="h-fit place-items-start">
          <div className="flex items-center gap-2">
            {icon}
            {label}
          </div>
        </Tab>
      ))}
    </TabsHeader>
  );
}

export default TabsHeaderComponent;
