import React from 'react'
import ReactECharts from "echarts-for-react";

function PieChart() {
    const colorPalette = ["#aaf683", "#ee6055"];
  const option = {
    title: {
      text: "Win-Lose Ratio",
      subtext: "util now",
      x: "center",
    },
    tooltip: {
      trigger: "item",
      formatter: "{a} <br/>{b} : {c} ({d}%)",
    },
    legend: {
      orient: "vertical",
      left: "left",
      data: ["Win", "Lose"],
    },
    series: [
      {
        name: "Percentage",
        type: "pie",
        radius: "55%",
        center: ["50%", "60%"],
        data: [
          { value: 80, name: "Win" },
          { value: 20, name: "Lose" },
        ],
        itemStyle: {
          emphasis: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: "rgba(0, 0, 0, 0.5)",
          },
        },
        color: colorPalette,
      },
    ],
  };

  function onChartClick(param, echarts) {
    console.log(param, echarts);
    alert(`name:${param.data.name}, value:${param.data.value}`);
  }

  function onChartLegendselectchanged(param, echarts) {
    console.log(param, echarts);
  }
  return (
    <ReactECharts
        option={option}
        className="w-full md:w-1/2"
        style={{ height: 500 }}
        onEvents={{
          click: onChartClick,
          legendselectchanged: onChartLegendselectchanged,
        }}
      />
  )
}

export default PieChart