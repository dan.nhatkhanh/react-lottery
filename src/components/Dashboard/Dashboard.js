import React from "react";
import PieChart from "./PieChart";
import GeneralData from "./GeneralData";

function Dashboard() {
  return (
    <div>
      <div className="mb-2">
        <h1 className="mt-5 text-3xl">Dashboard</h1>
        <p>View your overall report of your trades and transactions.</p>
      </div>
      <hr className="mb-4 h-[0.2rem] w-96 rounded border-0 bg-gray-300 md:mb-10" />
      <GeneralData />
      <PieChart />
    </div>
  );
}

export default Dashboard;
