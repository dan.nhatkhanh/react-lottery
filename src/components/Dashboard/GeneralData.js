import React, { useEffect, useState } from "react";
import axios from "axios";

import socketIO from "socket.io-client";
import { NotificationManager } from "react-notifications";
const socket = socketIO.connect("http://192.168.1.4:9898");

function GeneralData() {
  const [userDatas, setUserDatas] = useState([]);
  console.log(userDatas);
  useEffect(() => {
    socket.on("emitValue", (data) => {
      setUserDatas(data);
    });

    axios
      .get(`http://192.168.1.4:9898/users`)
      .then((res) => {
        console.log("a", res.data);
        setUserDatas(res.data);
      })
      .catch((err) => console.log(err));
  }, []);

  const createNotification = (type, message) => {
    return () => {
      switch (type) {
        case "info":
          NotificationManager.info(message);
          break;
        case "success":
          NotificationManager.success("Success message", "Title here");
          break;
        case "warning":
          NotificationManager.warning(
            "Warning message",
            "Close after 3000ms",
            3000
          );
          break;
        case "error":
          NotificationManager.error("Error message", "Click me!", 5000, () => {
            alert("callback");
          });
          break;
      }
    };
  };

  return (
    <div className="mb-20">
      <h1 className="py-2 text-2xl font-bold text-gray-700">General Data</h1>
      <div className="flex w-fit gap-5 rounded-xl border p-5 shadow-lg transition-shadow duration-200 hover:shadow-none">
        <div>
          <p className="font-semibold">
            User:{" "}
            <span className="font-normal text-green-500">
              {userDatas[0]?.username}
            </span>
          </p>
          <p className="font-semibold">
            Balance:{" "}
            <span className="font-normal text-green-500">
              {userDatas[0]?.balance}
            </span>
          </p>
          <p className="font-semibold">
            Stop Lost:{" "}
            <span className="font-normal text-green-500">
              {userDatas[0]?.stopLost}
            </span>
          </p>
          <p className="font-semibold">
            Take Profit:{" "}
            <span className="font-normal text-green-500">
              {userDatas[0]?.takeProfit}
            </span>
          </p>
          <p className="font-semibold">
            Lose Count:{" "}
            <span className="font-normal text-green-500">
              {userDatas[0]?.loseCount}
            </span>
          </p>
        </div>
        <div>
          <p className="font-semibold">
            Win Count:{" "}
            <span className="font-normal text-green-500">
              {userDatas[0]?.winCount}
            </span>
          </p>
          <p className="font-semibold">
            Profit Daily:{" "}
            <span className="font-normal text-green-500">
              {userDatas[0]?.profitDaily}
            </span>
          </p>
          <p className="font-semibold">
            Create At:{" "}
            <span className="font-normal text-green-500">
              {new Date(userDatas[0]?.createAt).toDateString()}
            </span>
          </p>
          <p className="font-semibold">
            Change At:{" "}
            <span className="font-normal text-green-500">
              {new Date(userDatas[0]?.changeAt).toDateString()}
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}

export default GeneralData;
