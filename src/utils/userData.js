export const userData = {
  username: "Nhat Truong",
  balance: "Not yet",
  stopLost: "Not yet",
  takeProfit: "Not yet",
  loseCount: 0,
  winCount: 0,
  profitDaily: "Not yet",
  createAt: { type: 0, default: Date.now() },
  changeAt: { type: 0, default: Date.now() },
};
