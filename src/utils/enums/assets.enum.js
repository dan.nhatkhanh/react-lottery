import NotfoundImage from "../../assets/img/404.jpg";

const ASSETS_ENUM = {
  NOT_FOUND_IMG: NotfoundImage,
};

export default ASSETS_ENUM;
