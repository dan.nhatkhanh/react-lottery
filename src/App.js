import { Route, Routes } from "react-router-dom";
import "react-notifications/lib/notifications.css";
import "./App.css";
import routes from "./pages/routes";

function App() {
  return (
    <Routes>
      {routes.map(({ component, path, ...rest }) => {
        return <Route key={path} path={path} element={component} />;
      })}
    </Routes>
  );
}

export default App;
